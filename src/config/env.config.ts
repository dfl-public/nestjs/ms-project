export const EnvConfiguration = () => ({
  mongodb: {
    url: process.env.APP_MONGODB_URL,
  },
  port: process.env.APP_PORT,
  prefix: process.env.APP_PREFIX,
});
