import * as Joi from 'joi';

export const JoiValidationSchema = Joi.object({
  //data base
  APP_MONGODB_URL: Joi.string().required(),

  //service
  APP_PORT: Joi.number().default(9000),

});
