import {
  CanActivate,
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { Reflector } from '@nestjs/core';

export const APPEND_SPACE_KEY = '__append_space_options__';

export type AppendSpaceOptions = {
  required?: boolean;
};

@Injectable()
export class AppendSpaceGuard implements CanActivate {
  constructor(
    private reflector: Reflector, // inyectamos el Reflector que es capaz de capturar los metadatas del contexto
  ) {}

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const req = context.switchToHttp().getRequest();
    const space = req.headers['x-workspace'];

    //obteniendo las options
    const options = this.reflector.get<AppendSpaceOptions>(
      APPEND_SPACE_KEY, // key que se pone en el SetMetadata
      context.getHandler(),
    );

    if (options.required && !space)
      throw new UnauthorizedException('missing space');

    req.body.space = space;

    return true;
  }
}
