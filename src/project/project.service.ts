import { Injectable, NotFoundException } from '@nestjs/common';
import { Project } from './entity/Project';

@Injectable()
export class ProjectService {
  private data: Project[] = [
    {
      id: '0',
      name: 'Ecommerce',
    },
    {
      id: '1',
      name: 'Modwell',
    },
    {
      id: '2',
      name: 'Biolinea',
    },
  ];

  create(project: Project): Project {
    project.id = new Date().getTime() + '';
    this.data.push(project);
    return project;
  }

  getOneByIndex(index: number): Project {
    const project = this.data[index];
    if (!project) throw new NotFoundException('Project not found');

    return project;
  }
}
