import { applyDecorators, SetMetadata, UseGuards } from '@nestjs/common';
import {
  APPEND_SPACE_KEY,
  AppendSpaceGuard,
  AppendSpaceOptions,
} from '../guards/append-space.guard';

export const AppendSpace = (options: AppendSpaceOptions) =>
  applyDecorators(
    SetMetadata(APPEND_SPACE_KEY, options),
    UseGuards(AppendSpaceGuard),
  );
