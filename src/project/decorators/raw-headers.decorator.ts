import { createParamDecorator, ExecutionContext } from '@nestjs/common';

export const RawHeaders = createParamDecorator(
  (header: string, ctx: ExecutionContext) => {
    const req = ctx.switchToHttp().getRequest();
    return !header ? req.headers : req.headers[header];
  },
);
