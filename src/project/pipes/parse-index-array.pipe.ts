import { BadRequestException, Injectable, PipeTransform } from '@nestjs/common';

@Injectable()
export class ParseIndexArrayPipe implements PipeTransform {
  transform(value: number) {
    if (value < 0) throw new BadRequestException('The index must be >= 0');
    return value;
  }
}
