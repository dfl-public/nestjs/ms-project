import {
  Body,
  Controller,
  Get,
  Param,
  ParseIntPipe,
  Post,
} from '@nestjs/common';
import { ProjectService } from './project.service';
import { Project } from './entity/Project';
import { ParseIndexArrayPipe } from './pipes/parse-index-array.pipe';
import { AppendSpace } from './decorators/append-space.decorator';

@Controller('project')
export class ProjectController {
  constructor(private readonly projectService: ProjectService) {}

  @Post()
  @AppendSpace({ required: true })
  create(@Body() project: Project) {
    return this.projectService.create(project);
  }

  @Get(':index')
  getOneByIndex(
    @Param('index', ParseIntPipe, ParseIndexArrayPipe) index: number,
  ) {
    return this.projectService.getOneByIndex(index);
  }
}
