import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Logger } from '@nestjs/common';

async function bootstrap() {
  const logger = new Logger('APPLICATOON');
  const port = 9000;

  const app = await NestFactory.create(AppModule);
  await app.listen(port);

  logger.log(`Application running in port : ${port}`);
}
bootstrap();
